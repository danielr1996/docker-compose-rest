FROM node:8
MAINTAINER Daniel Richter
EXPOSE 8080
RUN curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose
RUN docker-compose --version
COPY node_modules/ node_modules/
COPY index.js index.js
CMD ["node","index.js"]
