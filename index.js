const fs = require('fs');
const os = require('os');
const cp = require('child_process');
const path = require('path');

const express = require('express');
const bodyParser = require('body-parser')

const app = express();
const yamlParser = bodyParser.text({type: 'application/x-yaml'});

app.post('/services', yamlParser, (req, res) => {
    fs.writeFile(`${os.tmpdir()}${path.sep}docker-compose.yml`, req.body, (err) => {
        if (err) {
            res.sendStatus(500);
            return console.log(err);
        }
        startServices(`${os.tmpdir()}${path.sep}docker-compose.yml`);
        res.sendStatus(201);
    });
});

function startServices(composeFile) {
    console.log(cp.execSync(`docker-compose -f ${composeFile} up -d`).toString().trim());
}

app.listen(8080, () => {
    console.log('Listening on Port 8080');
})
